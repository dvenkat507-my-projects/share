var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        document.getElementById('name').addEventListener('focusin',this.emailFocusIn);
        document.getElementById('name').addEventListener('focusout',this.emailFocusOut);
    },
    emailFocusIn: function(){
        var orientation = (screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation;
        if (orientation === "landscape-primary" || orientation === "landscape-secondary"){
            document.getElementById('name_label').style.bottom = "0px";
        } else if (orientation === "portrait-secondary" || orientation === "portrait-primary"){
            document.getElementById('name_label').style.bottom = "0px";
        }
    },

    emailFocusOut: function(){
        var orientation = (screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation;
        var email = document.getElementById('name').value;
        if (orientation === "landscape-primary" || orientation === "landscape-secondary") {
            if(email.length > 0)
            {
                document.getElementById('name_label').style.bottom = "0px";
                document.getElementById('name_label').style.color = "Black";
            } else {
                document.getElementById('name_label').style.bottom = "-26px";
            }
        } else if (orientation === "portrait-secondary" || orientation === "portrait-primary") {
            if(email.length > 0) {
                document.getElementById('name_label').style.bottom = "0px";
                document.getElementById('name_label').style.color = "Black";
            } else {
                document.getElementById('name_label').style.bottom = "-26px";
            }
        }
    },
    onDeviceReady: function() {
    }
};

app.initialize();